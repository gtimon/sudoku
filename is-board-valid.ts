// this function is only to test that the test runner is working
export const sum = (a: number, b: number) => {
  return a + b;
};

type Board = any; // To be implemented

export const isBoardValid = (
  board: Board,
  row: number,
  col: number,
  num: number,
): boolean => {
  if (!isSubgridValid(board, row, col, num)) {
    return false;
  }

  if (!isRowValid(board, row, col, num)) {
    return false;
  }

  if (!isColValid(board, row, col, num)) {
    return false;
  }

  return true;
};

export const isSubgridValid = (
  board: Board,
  row: number,
  col: number,
  num: number,
): boolean => {
  throw new Error('Function not implemented.');
};

export const isRowValid = (
  board: Board,
  row: number,
  col: number,
  num: number,
): boolean => {
  throw new Error('Function not implemented.');
};

export const isColValid = (
  board: Board,
  row: number,
  col: number,
  num: number,
): boolean => {
  throw new Error('Function not implemented.');
};
