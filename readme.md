# Sudoku solver

## Sudoku

A sudoku is a typical game where you have to fill a 9x9 grid with numbers from 1 to 9. The grid is divided into 9 3x3 subgrids. The goal is to fill the grid so that each row, each column and each subgrid contains all the numbers from 1 to 9. For more information about sudoku, see [Wikipedia](https://en.wikipedia.org/wiki/Sudoku).

## Challenge

The challenge exposed here is to implement **one part** of a bigger sudoku solver strategy. This should consist of some functions and its correspondent unit tests. Also you need to define a model to represent the sudoku grid or board. We will nonetheless provide the desired signature of the functions that need to be implemented.

When adding a new number to the board, we will need to execute a method called isBoardValid, this method checks if the board is valid after inserting the number `num` in the `row` and `col` positions of the `board`. This function will also call another inner methods like this (by design):

```ts
function isBoardValid(
  board: Board,
  row: number,
  col: number,
  num: number,
): boolean {
  if (!isRowValid(board, row, col, num)) {
    return false;
  }

  if (!isColValid(board, row, col, num)) {
    return false;
  }

  if (!isSubgridValid(board, row, col, num)) {
    return false;
  }

  return true;
}
```

- You will need to implement the model `Board`, which should represent the sudoku board;
- You will need to implement the functions `isRowValid`, `isColValid` and `isSubgridValid`. The function `isBoardValid` is already implemented for you.
- The signature of all methods is the same, all get those arguments and should return a boolean;
- You will need to implement the unit tests for the functions `isRowValid`, `isColValid` and `isSubgridValid`, as well as for the given function `isBoardValid`.
- Optionally, would be great if you could implement the tests for `isBoardValid` as first step.

## Assumptions

- You can assume that the given number will be valid (1 to 9).
- You can assume that the board will always be valid before inserting the number. Meaning that no number will be repeated in the same row, column or subgrid.

## Notes

- There is already a set up with jest to run the tests, if you prefer another testing framework, feel free to change it (keep in mind that jest is a complete testing framework). Typescript is required though.
